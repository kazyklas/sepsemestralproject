#!/home/tklas/miniconda3/envs/sep/bin/python

from scipy import stats
import matplotlib.pyplot as plt
import numpy as np

# y represents Skoda Auto a.s. revenue in milions of CZK
y = np.array([177822, 189816, 211026, 188572, 170666, 203695, 231742, 239101, 246624, 299318, 314897, 347987, 407400, 416695])

# x represents sold car of the VW brand
x = np.array([1604470,1671551,1610684,1553488,1664443,1558848,1703915,1608647,1541890,1606145,1699221,1704547,1685053,1717108])

# z represents sold skoda cars
z = np.array([492111, 549667, 630032, 674530, 684226, 762600, 879200, 949412, 920800, 1037200, 1055500, 1127700, 1200500, 1253700])

coeficient  = np.corrcoef(x,y)
print(coeficient)

print(np.corrcoef(y,z))

slope, intercept, r_value, p_value, std_err = stats.linregress(y,z)
r_value = r_value**2
print(r_value)
plt.plot(y, z, 'o')
plt.plot(y, intercept + slope*y, 'r', label='')
plt.ylabel("Skoda Auto a.s. zisk")
plt.xlabel("Prodeje aut")
plt.legend()
plt.show()

